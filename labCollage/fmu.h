//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buNew;
	TButton *buClear;
	TButton *Button3;
	TToolBar *tbOptions;
	TButton *buNextImage;
	TButton *buPrevImage;
	TButton *buBringToFront;
	TButton *buSendToBack;
	TButton *buDel;
	TTrackBar *tbRotation;
	TLayout *ly;
	void __fastcall buNewClick(TObject *Sender);
	void __fastcall Selection1Change(TObject *Sender);
	void __fastcall buClearClick(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall buPrevImageClick(TObject *Sender);
	void __fastcall buNextImageClick(TObject *Sender);
	void __fastcall buBringToFrontClick(TObject *Sender);
	void __fastcall buSendToBackClick(TObject *Sender);
	void __fastcall buDelClick(TObject *Sender);
	void __fastcall tbRotationChange(TObject *Sender);
	void __fastcall SelectionAllMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
private:	// User declarations
	TSelection *FSel;
	void ReSetSelection(TObject *Sender);
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//--------------------------------------------------------------------------
#endif
