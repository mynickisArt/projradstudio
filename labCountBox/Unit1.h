//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *TabControl1;
	TTabItem *tMenu;
	TTabItem *tPlay;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TTabItem *TabItem5;
	TTabItem *TabItem6;
	TTabItem *TabItem7;
	TToolBar *ToolBar1;
	TButton *Button1;
	TLabel *Label1;
	TLabel *Label2;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buAnswer1;
	TButton *buAnswer2;
	TButton *buAnswer3;
	TButton *buAnswer4;
	TButton *buAnswer5;
	TButton *buAnswer6;
	TLayout *Layout1;
	TGridPanelLayout *GridPanelLayout2;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TRectangle *Rectangle3;
	TRectangle *Rectangle4;
	TRectangle *Rectangle5;
	TRectangle *Rectangle6;
	TRectangle *Rectangle7;
	TRectangle *Rectangle8;
	TRectangle *Rectangle9;
	TRectangle *Rectangle10;
	TRectangle *Rectangle11;
	TRectangle *Rectangle12;
	TRectangle *Rectangle13;
	TRectangle *Rectangle14;
	TRectangle *Rectangle15;
	TRectangle *Rectangle16;
	TRectangle *Rectangle17;
	TRectangle *Rectangle18;
	TRectangle *Rectangle19;
	TRectangle *Rectangle20;
	TRectangle *Rectangle21;
	TRectangle *Rectangle22;
	TRectangle *Rectangle23;
	TRectangle *Rectangle24;
	TRectangle *Rectangle25;
	TTimer *Timer1;
private:	// User declarations
	int FCountCorrect;
	int FCountWrong;
	double FTimeValues;
	Tlist *FlistBox;
	TList *FlistAnswer;
	void DoReset();
	void DoContinue();
	void DoAnswer (int aValue);
	void DoFinish();
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
const int cMaxBox = 25;
const int cMaxAnswer = 6;
const int cMinPossible = 4;
const int cMaxPossible = 14;
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
