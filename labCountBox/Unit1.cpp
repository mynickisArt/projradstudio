//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void_fastcall TForm1::FormCreate(TObject *Sender)
{
	Randomize();
	FListBox = new TList;
	for (int i = 1;<=cMaxBox;i++){
		FListBox->Add(this->FindComponent("Rectangle"+IntToStr(i)));
	}
	FListAnswer = new TList;
	FListAnswer->Add(buAnswer1);
	FListAnswer->Add(buAnswer2);
	FListAnswer->Add(buAnswer3);
	FListAnswer->Add(buAnswer4);
	FListAnswer->Add(buAnswer5);
	FListAnswer->Add(buAnswer6);
}
void_fastcall TForm1::FormDestroy(TObject *Sender)
{
	delete FListBox;
	delete FListAnswer;
}
//---------------------------------------------------------------------------
void Tfm::DoReset()
{
	FCountCorrect = 0;
	FCountWrong = 0;
	FTimevalue = Time().val + (double)30/(24*60*60);
	tmPlay->Enabled = true;
	DoContinue();
}
//---------------------------------------------------------------------------
void Tfm::DoContinue()
{
	for (i =0;i < cMaxBox; i++) {
		((TRectangle*)FListBox->Items[i])->Fill->Color = AlphaColorRec::LightGray;
	}
	FNumberCorrect = RandomRange(cMinPossible, cMaxPossible);
	int *x = RandomArrayUnique(cMinBox, FNumberCorrect);
		for (i =0;i < FNumberCorrect; i++) {
		((TRectangle*)FListBox->Items[i])->Fill->Color = AlphaColorRec::LightBlue;
	}

	int xAnswerStart - FNumberCorrect - Random(cMaxAnswer-1);
	if(AnswerStart<cMinPossible)
	xAnswerStart < cMinPosssible;

	for (i = 0; i < cMaxAnswer; i++) {
		((TButton*)FListAnswer->Items[i])->Text = IntToStr(xAnswerStart + i);
	}


}
//---------------------------------------------------------------------------
void Tfm::DoAnswer(int aValue)
{
	(aValue == FNumberCorrect) ? FCountCorrect++ : FCountWrong++;
	if (FCountWrong > 5) {
		//
		DoContinue();
	}
}
//---------------------------------------------------------------------------
void Tfm::DoFinish()
{
	tmPlay->Enabled = false;
}
void_fastcall Tfm::buAnswerAllClick(TObject *Sender)
{
	DoAnswer(StrToInt(((TButton*)Sender)->Text));
}
void_fastcall Tfm::tmPlayTimer(TObject *Sender)
{
	double x = FTimeValue - Time().Val;
	laTime->Text = FormatDataTime(
	if (n<=o)
		DoFinish();
	}
}

