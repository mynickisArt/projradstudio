//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ImgList.hpp>
#include <System.ImageList.hpp>
#include <FMX.TabControl.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TLayout *Layout2;
	TLayout *Layout3;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TLabel *Label4;
	TImage *Image1;
	TImageList *iStar;
	TImageList *iBall;
	TImageList *iMish;
	TImageList *iGift;
	TButton *Button1;
	TStyleBook *StyleBook1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TButton *Button6;
	TButton *Button7;
	TButton *Button8;
	TButton *Button9;
	TButton *Button10;
	TButton *Button11;
	TGlyph *glStar;
	TGlyph *glBall;
	TGlyph *glBall3;
	TGlyph *glBall6;
	TGlyph *glBall4;
	TGlyph *glBall5;
	TGlyph *glBall7;
	TGlyph *glBall2;
	TGlyph *glMish;
	TGlyph *glMish2;
	TGlyph *glGift;
	TTabControl *TabControl1;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TButton *Button12;
	TButton *Button13;
	void __fastcall ButtonStarAllClick(TObject *Sender);
	void __fastcall ButtonBallAllClick(TObject *Sender);
	void __fastcall ButtonMishAllClick(TObject *Sender);
	void __fastcall ButtonGiftAllClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
