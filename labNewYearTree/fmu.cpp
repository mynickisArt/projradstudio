//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ButtonStarAllClick(TObject *Sender)
{
	glStar->ImageIndex = ((TButton*)Sender)->ImageIndex;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ButtonBallAllClick(TObject *Sender)
{
	glBall->ImageIndex = ((TButton*)Sender)->ImageIndex;
	glBall2->ImageIndex = ((TButton*)Sender)->ImageIndex;
	glBall3->ImageIndex = ((TButton*)Sender)->ImageIndex;
	glBall4->ImageIndex = ((TButton*)Sender)->ImageIndex;
	glBall5->ImageIndex = ((TButton*)Sender)->ImageIndex;
	glBall6->ImageIndex = ((TButton*)Sender)->ImageIndex;
	glBall7->ImageIndex = ((TButton*)Sender)->ImageIndex;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ButtonMishAllClick(TObject *Sender)
{
	glMish->ImageIndex = ((TButton*)Sender)->ImageIndex;
	glMish2->ImageIndex = ((TButton*)Sender)->ImageIndex;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ButtonGiftAllClick(TObject *Sender)
{
    glGift->ImageIndex = ((TButton*)Sender)->ImageIndex;
}
//---------------------------------------------------------------------------
