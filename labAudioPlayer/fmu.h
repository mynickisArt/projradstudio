//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.Media.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <system.IOUtils.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLayout *lyTop;
	TLayout *lyCurrentTime;
	TLayout *lyBottom;
	TLabel *laPath;
	TTimer *tm;
	TMediaPlayer *mp;
	TListView *lv;
	TButton *buAbout;
	TLabel *laTitle;
	TTrackBar *tbCurrentTime;
	TLabel *laCurrentTime;
	TLabel *laDuration;
	TButton *buPlay;
	TButton *buPause;
	TButton *buStop;
	TButton *buPrev;
	TButton *buNext;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall lvItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall buPlayClick(TObject *Sender);
	void __fastcall tmTimer(TObject *Sender);
	void __fastcall tbCurrentTimeChange(TObject *Sender);
	void __fastcall buPauseClick(TObject *Sender);
	void __fastcall buStopClick(TObject *Sender);
	void __fastcall buPrevClick(TObject *Sender);
	void __fastcall buNextClick(TObject *Sender);

private:	// User declarations
	unsigned int FPauseTime;
	void DoRefreshList(UnicodeString aPath);
	void DoStop();
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
